package org.example;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.GenericApplicationContext;

public class PostConstructDemo {

   @Configuration
   static class Config {

      @Lazy
      @Bean(initMethod="init")
      Person personOne() {
         final Person personOne = new Person();
         personOne.setName("TisóFeri");
         return personOne;
      }

   }

   public static void main(String[] args) {
      final GenericApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

      Person.getBean("personOne", context);

      context.close();
   }

}
