package org.example;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.BeanCreationException;

import org.springframework.context.ApplicationContext;

public class Person {

   private static final String DEFAULT_NAME = "Minta Béla";

   private String name;

   public void setName(final String name) {
      this.name = name;
   }

   // protected: called once; package p.: called once; public: called once; private: called twice
   @PostConstruct
   public void init() throws Exception {
      System.out.println("Initializing bean.");

      if (name == null) {
         System.out.println("Using default name.");
         name = DEFAULT_NAME;
      }
   }

   public String toString() {
      return "\tName: " + name;
   }

   public static Person getBean(final String beanName, final ApplicationContext context) {
      try {
         final Person bean = (Person) context.getBean(beanName);
         System.out.println(bean);
         return bean;
      } catch (final BeanCreationException bce) {
         System.out.println("An error occured in bean configuration: " + bce.getMessage());
         return null;
      }
   }
}
